﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class Enemy : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnBeforeLevelLoaded += OnBeforeLevelLoaded_Action;
    }

    private void OnDestroy()
    {
        Events.OnBeforeLevelLoaded -= OnBeforeLevelLoaded_Action;
    }

    private void OnBeforeLevelLoaded_Action(object sender, EventArgs e)
    {
        Destroy(gameObject);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Player")
        {
            Events.OnLost_FireEvent();
        }
    }

}