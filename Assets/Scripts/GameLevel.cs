﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameLevel
{

    private int levelCurrent;
    private string levelJSON;
    private Level level;

    public GameLevel()
    {
        levelCurrent = 1;
        LoadLevelFromFile();
        CreateLevel();
    }

    public LevelEventData CurrentLevelEventData()
    {
        return new LevelEventData
        {
            levelCurrent = levelCurrent,
            levelMax = level.fields.Length,
            field = level.fields[levelCurrent - 1],
        };
    }

    private void LoadLevelFromFile()
    {
        levelJSON = Resources.Load<TextAsset>("Levels/game").ToString();
    }

    private void CreateLevel()
    {
        level = JsonUtility.FromJson<Level>(levelJSON);
    }

    public bool NextLevel()
    {
        if (levelCurrent < level.fields.Length)
        {
            levelCurrent++;
            return true;
        }
        return false;
    }

}

[Serializable]
public class Level
{
    public Field[] fields;
}

[Serializable]
public class Field
{
    public Size size;
    public Goal goal;
    public Place place;
    public Figure[] figures;
}

[Serializable]
public class Place
{
    public Vector2 position;
    public float size;
}

[Serializable]
public class Figure
{
    public Vector2 position;
    public float size;
}

[Serializable]
public class Size
{
    public float width, height;
}

[Serializable]
public class Goal
{
    public float size, posx;
}
