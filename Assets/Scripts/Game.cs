﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using GameAnalyticsSDK;
public class Game : CryoBehaviour
{
    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("FiguresContainer")]
    private GameObject FiguresContainer { get; set; }

    public GameObject enemy;

    private GameLevel gameLevel;

    void Start()
    {
        GameAnalytics.Initialize();

        Events.OnGameStart += OnGameStart_Action;
        Events.OnGameEnd += OnGameEnd_Action;
        Events.OnGoal += OnGoal_Action;
        Events.OnLost += OnLost_Action;
        Events.OnGameRestart += OnGameRestart_Action;

        StartTheGame();
    }

    private void OnGameStart_Action(object sender, EventArgs e)
    {
        StartTheGame();
    }

    private void OnGoal_Action(object sender, EventArgs e)
    {
        GameAnalytics.NewDesignEvent("WinLevel", gameLevel.CurrentLevelEventData().levelCurrent);
        NextLevel();
    }

    private void OnLost_Action(object sender, EventArgs e)
    {
        RestartLevel();
    }

    private void NextLevel()
    {
        Events.OnBeforeLevelLoaded_FireEvent();
        if (gameLevel.NextLevel())
        {
            Events.OnLevelLoaded_FireEvent(gameLevel.CurrentLevelEventData());
        } else
        {
            Events.OnGameEnd_FireEvent();
        }
    }

    private void RestartLevel()
    {
        Events.OnBeforeLevelLoaded_FireEvent();
        Events.OnLevelLoaded_FireEvent(gameLevel.CurrentLevelEventData());
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        Debug.Log("GAME ENDED");
    }

    private void OnGameRestart_Action(object sender, EventArgs e)
    {
        StartTheGame();
    }

    private void StartTheGame()
    {
        CreateLevel();
    }
    private void CreateLevel()
    {
        gameLevel = new GameLevel();
        StartCoroutine(OnCreateFirstLevelDelayTimer());
    }

    IEnumerator OnCreateFirstLevelDelayTimer()
    {
        yield return new WaitForSeconds(0f);
        Events.OnLevelLoaded_FireEvent(gameLevel.CurrentLevelEventData());
    }

}
