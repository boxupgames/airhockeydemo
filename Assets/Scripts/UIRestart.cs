﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class UIRestart : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }


    void Start()
    {
        Events.OnGameEnd += OnGameEnd_Action;
        gameObject.SetActive(false);
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(true);
    }

    public void OnClick()
    {
        StartCoroutine(RestartDelay());
    }

    IEnumerator RestartDelay()
    {
        yield return new WaitForSeconds(.5f);
        Events.OnGameRestart_FireEvent();
        gameObject.SetActive(false);
    }

}
