﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class FieldPlay : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("FiguresContainer")]
    private GameObject FiguresContainer { get; set; }

    public GameObject enemy;

    private GameObject border;
    private GameObject goal;
    private float fieldMargin = .25f;

    private float horizontalHalf;
    private float horizontalFull;
    private float verticalHalf;
    private float verticalFull;

    void Start()
    {
        Events.OnLevelLoaded += OnLevelLoaded_Action;
        border = GameObject.Find("FieldBorder");
        goal = GameObject.Find("FieldGoal");
    }

    private void OnLevelLoaded_Action(object sender, GameEvents.GameLevelEventArgs e)
    {
        PrepareField(e._levelData.field);
        BuildFigures(e._levelData.field.figures);
    }

    private void BuildFigures(Figure[] figures)
    {
        foreach (Figure figure in figures)
        {
            InstantiateFigure(figure);
        }
    }

    private void InstantiateFigure(Figure figure)
    {

        float widthFull = GetComponent<SpriteRenderer>().bounds.size.x;
        float widthHalf = widthFull / 2;
        float figurePosX = (figure.position.x <= .5f) ? (-widthHalf + (widthFull * figure.position.x)) : ((widthFull * figure.position.x) - widthHalf);

        float heightFull = GetComponent<SpriteRenderer>().bounds.size.y;
        float heightHalf = heightFull / 2;
        float figurePosY = (figure.position.y <= .5f) ? (-heightHalf + (heightFull * figure.position.y)) : ((heightFull * figure.position.y) - heightHalf);

        GameObject f = Instantiate(enemy, new Vector2(figurePosX, figurePosY), Quaternion.identity, FiguresContainer.transform);
        f.transform.localScale = new Vector2(figure.size, figure.size);
    }

    private void PrepareField(Field field)
    {
        horizontalHalf = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0f, 0f)).x;
        horizontalFull = horizontalHalf * 2;
        verticalHalf = Camera.main.ScreenToWorldPoint(new Vector3(0f, Screen.height, 0f)).y;
        verticalFull = verticalHalf * 2;

        float fieldWidth = (horizontalFull * field.size.width) - fieldMargin;
        float fieldHeight = (verticalFull * field.size.height) - fieldMargin;

        Vector2 fieldSize = new Vector2(fieldWidth, fieldHeight);
        ResizeFieldBackground(fieldSize);
        ResizeFieldBorder(fieldSize);
        MoveFieldGoal(field.goal);

        Events.OnFieldResized_FireEvent(fieldSize, field.place.position);
    }

    private void ResizeFieldBackground(Vector2 fieldSize)
    {
        GetComponent<SpriteRenderer>().size = fieldSize;
    }

    private void ResizeFieldBorder(Vector2 fieldSize)
    {
        border.GetComponent<SpriteRenderer>().size = fieldSize;
      //  border.AddComponent<PolygonCollider2D>().autoTiling = true;
    }

    private void MoveFieldGoal(Goal _goal)
    {
        float widthFull = GetComponent<SpriteRenderer>().bounds.size.x;
        float widthHalf = widthFull / 2;
        float goalPosX = (_goal.posx <= .5f) ? (-widthHalf + (widthFull * _goal.posx)) : ((widthFull * _goal.posx) - widthHalf);

        goal.transform.localScale = new Vector2(_goal.size, 1f);

        float scaleOneHeight = (Math.Abs(GetComponent<SpriteRenderer>().bounds.size.y / transform.localScale.y) / 2) - (goal.GetComponent<SpriteRenderer>().bounds.size.y * 3) + fieldMargin;

        goal.transform.localPosition = new Vector2(goalPosX, scaleOneHeight);
    }


}
