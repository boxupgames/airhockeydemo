﻿using UnityEngine;
using CryoDI;

public class GameContainers : UnityStarter
{

    protected override void SetupContainer(CryoContainer container)
    {
        container.RegisterSingleton<GameEvents>(LifeTime.Global);

        container.RegisterSceneObject<GameObject>("FieldGroup/FieldPlay", "FieldPlay", LifeTime.Scene);
        container.RegisterSceneObject<GameObject>("FieldGroup/FieldPlay/FiguresContainer", "FiguresContainer", LifeTime.Scene);
    }

}
