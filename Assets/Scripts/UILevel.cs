﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class UILevel : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnLevelLoaded += OnLevelLoaded_Action;
    }

    private void OnLevelLoaded_Action(object sender, GameEvents.GameLevelEventArgs e)
    {
        GetComponent<Text>().text = e._levelData.levelCurrent.ToString();
    }

}
