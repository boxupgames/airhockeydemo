﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class Border : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Player")
        {
            Events.OnGoal_FireEvent();
        }
    }

}
