﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class UIMessage : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    private IEnumerator timerCoroutine;

    void Start()
    {
        Events.OnLevelLoaded += OnLevelLoaded_Action;
        Events.OnSwipe += OnSwipe_Action;
        Events.OnGoal += OnGoal_Action;
        Events.OnLost += OnLost_Action;
        Events.OnGameEnd += OnGameEnd_Action;

        gameObject.SetActive(false);
    }

    private void OnLevelLoaded_Action(object sender, GameEvents.GameLevelEventArgs e)
    {
        if (e._levelData.levelCurrent == 1)
        {
            GetComponent<Text>().text = "Свайпни для начала";
        } else
        {
            GetComponent<Text>().text = "Уровень " + e._levelData.levelCurrent;
        }
        gameObject.SetActive(true);
    }

    private void OnSwipe_Action(object sender, GameEvents.SwipeEventArgs e)
    {
        gameObject.SetActive(false);
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        GetComponent<Text>().text = "Победа!";
        gameObject.SetActive(true);
        StopCoroutine(timerCoroutine);
    }
    private void OnGoal_Action(object sender, EventArgs e)
    {
        GetComponent<Text>().text = "Гол!";
        gameObject.SetActive(true);
        timerCoroutine = HideMessageDelayTimer();
        StartCoroutine(timerCoroutine);
    }
    private void OnLost_Action(object sender, EventArgs e)
    {
        GetComponent<Text>().text = "Промах!";
        gameObject.SetActive(true);
        timerCoroutine = HideMessageDelayTimer();
        StartCoroutine(timerCoroutine);
    }
    IEnumerator HideMessageDelayTimer()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }


}
