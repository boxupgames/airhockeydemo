﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class Player : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("FieldPlay")]
    private GameObject FieldPlay { get; set; }

    private Vector2 firstPressPos;
    private Vector2 secondPressPos;
    private Vector2 currentSwipe;

    private bool canSwipe;

    // Start is called before the first frame update
    void Start()
    {
        Events.OnSwipe += OnSwipe_Action;
        Events.OnLevelLoaded += OnLevelLoaded_Action;
        Events.OnFieldResized += OnFieldResized_Action;
        Events.OnGoal += OnGoal_Action;
        Events.OnLost += OnLost_Action;
        Events.OnGameRestart += OnGameRestart_Action;
        Events.OnGameEnd += OnGameEnd_Action;

        gameObject.transform.localScale = new Vector3(.5f, .5f);
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (canSwipe)
            SwipeMouse();
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        if (canSwipe)
            SwipeMobile();
#endif
    }

    private void OnLevelLoaded_Action(object sender, GameEvents.GameLevelEventArgs e)
    {
        Vector3 playerPosition = e._levelData.field.place.position;

        float widthFull = FieldPlay.GetComponent<SpriteRenderer>().bounds.size.x;
        float widthHalf = widthFull / 2;
        float figurePosX = (playerPosition.x <= .5f) ? (-widthHalf + (widthFull * playerPosition.x)) : ((widthFull * playerPosition.x) - widthHalf);

        float heightFull = FieldPlay.GetComponent<SpriteRenderer>().bounds.size.y;
        float heightHalf = heightFull / 2;
        float figurePosY = (playerPosition.y <= .5f) ? (-heightHalf + (heightFull * playerPosition.y)) : ((heightFull * playerPosition.y) - heightHalf);

        gameObject.transform.localScale = new Vector2(e._levelData.field.place.size, e._levelData.field.place.size);
        gameObject.transform.localPosition = new Vector2(figurePosX, figurePosY);

        canSwipe = true;
    }

    private void OnFieldResized_Action(object sender, GameEvents.FieldSizeEventArgs e)
    {
        float widthFull = e._fieldSize.x;
        float widthHalf = widthFull / 2;
        float figurePosX = (e._playerPos.x <= .5f) ? (-widthHalf + (widthFull * e._playerPos.x)) : ((widthFull * e._playerPos.x) - widthHalf);

        float heightFull = e._fieldSize.y;
        float heightHalf = heightFull / 2;
        float figurePosY = (e._playerPos.y <= .5f) ? (-heightHalf + (heightFull * e._playerPos.y)) : ((heightFull * e._playerPos.y) - heightHalf);

        gameObject.transform.localPosition = new Vector2(figurePosX, figurePosY);
    }

    public void SwipeMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            Events.OnSwipe_FireEvent(currentSwipe);
        }
    }

    public void SwipeMobile()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                secondPressPos = new Vector2(t.position.x, t.position.y);
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                Events.OnSwipe_FireEvent(currentSwipe);
            }
        }
    }

    private void OnSwipe_Action(object sender, GameEvents.SwipeEventArgs e)
    {
        GetComponent<Rigidbody2D>().AddForce(e._vector);
    }

    private void OnGoal_Action(object sender, EventArgs e)
    {
        ResetVelocity();
    }

    private void OnLost_Action(object sender, EventArgs e)
    {
        ResetVelocity();
    }

    private void OnGameRestart_Action(object sender, EventArgs e)
    {
        ResetVelocity();
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        canSwipe = false;
    }

    private void ResetVelocity()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector3();
    }

}
