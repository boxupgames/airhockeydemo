﻿using System;
using UnityEngine;

public class GameEvents
{

    public event EventHandler OnGameLaunched;
    public event EventHandler OnGameRestart;
    public event EventHandler OnGameStart;
    public event EventHandler OnGameEnd;
    public event EventHandler OnGoal;
    public event EventHandler OnLost;
    public event EventHandler OnBeforeLevelLoaded;
    
    public event FieldSizeHandler OnFieldResized;
    
    public event SwipeEventHandler OnSwipe;

    public event GameLevelEventHandler OnLevelLoaded;

    public event OnWindowOrientationChangeEventHandler OnWindowOrientationChange;



    public delegate void SwipeEventHandler(object sender, SwipeEventArgs e);
    public void OnSwipe_FireEvent(Vector2 vector)
    {
        SwipeEventArgs args = new SwipeEventArgs(vector);
        OnSwipe?.Invoke(this, args);
    }
    public class SwipeEventArgs : EventArgs
    {
        public Vector2 _vector { get; set; }
        public SwipeEventArgs(Vector2 vector)
        {
            _vector = vector;
        }
    }

    public delegate void OnWindowOrientationChangeEventHandler(object sender, WindowOrientationChangeEventArgs e);
    public void OnWindowOrientationChange_FireEvent(bool isLandscape)
    {
        WindowOrientationChangeEventArgs args = new WindowOrientationChangeEventArgs(isLandscape);
        OnWindowOrientationChange?.Invoke(this, args);
    }
    public class WindowOrientationChangeEventArgs : EventArgs
    {
        public bool _isLandscape { get; set; }
        public WindowOrientationChangeEventArgs(bool isLandscape)
        {
            _isLandscape = isLandscape;
        }
    }

    public delegate void GameLevelEventHandler(object sender, GameLevelEventArgs e);
    public void OnLevelLoaded_FireEvent(LevelEventData levelData)
    {
        GameLevelEventArgs args = new GameLevelEventArgs(levelData);
        OnLevelLoaded?.Invoke(this, args);
    }
    public class GameLevelEventArgs : EventArgs
    {
        public LevelEventData _levelData { get; set; }
        public GameLevelEventArgs(LevelEventData levelData)
        {
            _levelData = levelData;
        }
    }

    public delegate void FieldSizeHandler(object sender, FieldSizeEventArgs e);
    public void OnFieldResized_FireEvent(Vector2 fieldSize, Vector2 playerPos)
    {
        FieldSizeEventArgs args = new FieldSizeEventArgs(fieldSize, playerPos);
        OnFieldResized?.Invoke(this, args);
    }
    public class FieldSizeEventArgs : EventArgs
    {
        public Vector2 _fieldSize { get; set; }
        public Vector2 _playerPos { get; set; }
        public FieldSizeEventArgs(Vector2 fieldSize, Vector2 playerPos)
        {
            _fieldSize = fieldSize;
            _playerPos = playerPos;
        }
    }



    public void OnGameLaunched_FireEvent()
    {
        OnGameLaunched?.Invoke(this, EventArgs.Empty);
    }

    public void OnGameStart_FireEvent()
    {
        OnGameStart?.Invoke(this, EventArgs.Empty);
    }

    public void OnGameEnd_FireEvent()
    {
        OnGameEnd?.Invoke(this, EventArgs.Empty);
    }

    public void OnGoal_FireEvent()
    {
        OnGoal?.Invoke(this, EventArgs.Empty);
    }

    public void OnLost_FireEvent()
    {
        OnLost?.Invoke(this, EventArgs.Empty);
    }

    public void OnBeforeLevelLoaded_FireEvent()
    {
        OnBeforeLevelLoaded?.Invoke(this, EventArgs.Empty);
    }

    public void OnGameRestart_FireEvent()
    {
        OnGameRestart?.Invoke(this, EventArgs.Empty);
    }

}
